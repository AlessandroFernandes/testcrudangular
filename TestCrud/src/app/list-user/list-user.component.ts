import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel as User } from '../model/user.model';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users: User[] = [];
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.userService.getUser()
      .subscribe( data => {
          this.users = data;
      })
  }

  delete(user: User){
    this.userService.deleteUser(user.id)
      .subscribe( data => {
          this.users = this.users.filter(us => us !== user)
      })
  }

  editUser(user: User){
    localStorage.removeItem("editUserId");
    localStorage.setItem("editUserId", user.id.toString());
    this.router.navigate(['edit-user']);
  }

  addUser():void{
    this.router.navigate(['add-user']);
  }
}
