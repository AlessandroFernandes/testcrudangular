import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel as User} from '../model/user.model';
import { Observable } from 'rxjs';


@Injectable({providedIn: 'root'})
export class UserService {

  constructor(private Http: HttpClient) {}

  API: string = "//localhost:5000/fakeUser";

  getUser(){
    return this.Http.get<User[]>(this.API);
  }

  getUserById(id: number){
    return this.Http.get<User[]>(this.API + '/' + id);
  }

  createUser(user: any): Observable<User[]>{
    return this.Http.post<User[]>(this.API, user);
  }

  updateUser(user: any){
    return this.Http.get<User[]>(this.API + '/' + user.id, user);
  }

  deleteUser(id: number){
    return this.Http.get<User[]>(this.API + '/' + id);
  }

}
